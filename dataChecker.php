<?php

class dataChecker
{
	protected $data;
	protected $url = "/index.php";
	protected $errors = [];
	protected $file_name;

	public function __construct()
	{
		require_once(__DIR__.'/generateImageId.php');
		require_once(__DIR__.'/db/QueryBuilder.php');

		$this->gid = (new generateImageId())->run();
		$this->QueryBuilder = new QueryBuilder();
	}

	public function check($pdoType, $id = null)
	{
	  if(isset($_POST['title']) && isset($_FILES['image']))
	  {
		if(empty($_POST['title']))
		{
			$this->redirect();
		}

		if(empty($_FILES['image']))
		{
			$this->redirect();
		}

		$this->checkImage($pdoType, $id);
	  }
	}

	protected function redirect()
	{
		header('Location: '.$this->url);
	}

	protected function checkImage($pdoType, $id)
	{
	 if(isset($_FILES['image'])){
      $errors= array();
      $this->file_name = $_FILES['image']['name'];
      $file_size = $_FILES['image']['size'];
      $file_tmp = $_FILES['image']['tmp_name'];
      $file_type = $_FILES['image']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
      
      $extensions= array("jpeg","jpg","png");
      
      if(in_array($file_ext,$extensions)=== false){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      }
      
      if($file_size > 2097152) {
         $errors[]='File size must be excately 2 MB';
      }
      
      if(empty($errors)==true) {
         move_uploaded_file($file_tmp,"images/".$this->gid.'.'.$file_ext);
          "Your image have been uploaded.";
			$this->QueryBuilder->$pdoType([
				'title' => $_POST['title'],
				'gid' => $this->gid,
				'file_name' => $this->file_name,
				'id' => $id
			]);
      }else{
         print_r($errors);
      }
   }
	}
}