<?php
require_once (__DIR__.'/db/QueryBuilder.php');
require_once (__DIR__.'/dataChecker.php');

if(isset($_GET['id']))
{
	$id = $_GET['id'];

	$QueryBuilder = new QueryBuilder();
	$row = $QueryBuilder->selectSingle($id);

	if (isset($_POST['submit']))
	{
		$data = new dataChecker();
		$data = $data->check("update", $row['id']);
		$ext = explode('.', $row['file_name']);

		unlink(__DIR__.'/images/'.$row['gid'].'.'.$ext[1]);

		header("Location:".'/list.php');
	}

}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit</title>
</head>
<body>
<form action="" method="POST" enctype="multipart/form-data">
	<input type="input" value="<?=$row['title']?>"name="title" placeholder="Title"><br>
	<input type="file" name="image"><br> 
	<button name="submit">Submit</button>
	</form>
</body>
</html>