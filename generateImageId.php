<?php

class generateImageId
{
	protected $string;
	protected $length = 5;

	public function run()
	{
		    $keys = array_merge(range(0, 9), range('a', 'z'));

		    for ($i = 0; $i < $this->length; $i++) {
		        $this->string .= $keys[array_rand($keys)];
		    }

		    return $this->string;
	}
}