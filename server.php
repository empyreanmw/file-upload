<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once (__DIR__.'/db/connection.php');
require_once (__DIR__.'/dataChecker.php');

$connection = new Connection();

$data = new dataChecker();

$data->check();
