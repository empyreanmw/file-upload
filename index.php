
<?php

require_once (__DIR__.'/db/connection.php');
require_once (__DIR__.'/dataChecker.php');
require_once (__DIR__.'/db/QueryBuilder.php');

$connection = new Connection();

$data = new dataChecker();

$data = $data->check("insert");

?>

<!DOCTYPE html>
<html>
<head>
	<title>File Upload</title>
</head>
<body>
	<ul>
		<li><a href="/index.php">Home</a></li>
		<li><a href="/list.php">List</a></li>
	</ul>
	<form action="" method="POST" enctype="multipart/form-data">
	<input type="input" name="title" placeholder="Title"><br>
	<input type="file" name="image"> <br>
	<button>Submit</button>
	</form>
</body>
</html>