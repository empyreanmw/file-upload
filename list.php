<?php
require_once (__DIR__.'/db/QueryBuilder.php');

$QueryBuilder = new QueryBuilder();

$data = $QueryBuilder->selectAll();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit</title>
	<style>
		table {
		  font-family: arial, sans-serif;
		  border-collapse: collapse;
		  width: 100%;
		}

		td, th {
		  border: 1px solid #dddddd;
		  text-align: left;
		  padding: 8px;
		}

		tr:nth-child(even) {
		  background-color: #dddddd;
		}
</style>
</head>
<body>
	<ul>
		<li><a href="/index.php">Home</a></li>
		<li><a href="/list.php">List</a></li>
	</ul>
	<table>
  <tr>
    <th>Title</th>
    <th>File Name</th>
    <th>Gid</th>
    <th>Edit</th>
    <th>Delete</th>
  </tr>
  <?php
  	foreach ($data as $row) {
  		 echo "<tr>
				    <td>{$row['title']}</td>
				    <td>{$row['file_name']}</td>
				    <td>{$row['gid']}</td>
				    <td><a href=\"/delete.php?id={$row['id']}\">Delete</a> </td>
				    <td><a href=\"/edit.php?id={$row['id']}\">Edit</a> </td>
 			  </tr>";
  	}
  ?>
</table>
</body>
</html>