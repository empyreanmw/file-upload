<?php
require_once (__DIR__.'/db/QueryBuilder.php');

if(isset($_GET['id']))
{
	$id = $_GET['id'];

	$QueryBuilder = new QueryBuilder();

	$row = $QueryBuilder->selectSingle($id);
	$ext = explode('.', $row['file_name']);

	unlink(__DIR__.'/images/'.$row['gid'].'.'.$ext[1]);

	$QueryBuilder->delete($id);

	header("Location:"."/list.php");

}