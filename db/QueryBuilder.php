<?php

class QueryBuilder
{
	protected $pdo;

	public function __construct()
	{
		require_once('connection.php');

		$this->pdo = (new Connection)->getPDO();
		$this->pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );	
	}

	public function insert($data)
	{
		$query = "INSERT INTO fileUpload (gid, title, file_name)
				VALUES (:gid, :title, :file_name)";
		$stmt = $this->pdo->prepare($query);
		$stmt->bindParam(':gid', $data['gid']);
		$stmt->bindParam(':title', $data['title']);
		$stmt->bindParam(':file_name', $data['file_name']);
		$stmt->execute();
	}

	public function selectAll()
	{
		$data = $this->pdo->query("SELECT id, file_name, gid, title FROM fileUpload")->fetchAll();

		return $data;
	}

	public function selectSingle($id)
	{
		$stmt = $this->pdo->prepare("SELECT id, file_name, gid, title FROM fileUpload WHERE id= ? LIMIT 1");

		$stmt->execute(array($id)); 

		return $stmt->fetch();
	}

	public function delete($id)
	{
		$query = "DELETE FROM fileUpload WHERE id = ?";        
		$smtp = $this->pdo->prepare($query);

		$smtp->execute(array($id));  
	}

	public function update($data)
	{
		$query = "UPDATE fileUpload SET gid=:gid, title=:title, file_name=:file_name WHERE id=:id";
		$stmt = $this->pdo->prepare($query);
		$stmt->bindParam(':gid', $data['gid']);
		$stmt->bindParam(':id', $data['id']);
		$stmt->bindParam(':title', $data['title']);
		$stmt->bindParam(':file_name', $data['file_name']);
		$stmt->execute();
	}
}
