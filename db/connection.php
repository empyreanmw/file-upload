<?php

class Connection
{
	protected $pdo;
	protected $server_name = "localhost";
	protected $db_name = "file_upload";
	protected $user_name = "root";
	protected $password = "root";

	public function __construct()
	{
		$this->pdo = new \PDO("mysql:host=$this->server_name;dbname=$this->db_name", $this->user_name, $this->password);
	}

	public function getPDO()
	{
		return $this->pdo;
	}
}
	
